using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evklid.Tests
{
    [TestClass]
    public class GreatestCommonDivisorTests
    {
        [TestMethod]
        public void Euclidean_180and111_3return()
        {
            int value1 = 180;
            int value2 = 111;
            int expected = 3;

            int actual = GreatestCommonDivisor.Euclidean(value1, value2);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Euclidean_180and240and40_20return()
        {
            int value1 = 180;
            int value2 = 240;
            int value3 = 40;
            int expected = 20;

            int actual = GreatestCommonDivisor.Euclidean(value1, value2, value3);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Euclidean_180and240and40and60_20return()
        {
            int value1 = 180;
            int value2 = 240;
            int value3 = 40;
            int value4 = 60;
            int expected = 20;

            int actual = GreatestCommonDivisor.Euclidean(value1, value2, value3, value4);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Euclidean_180and240and40and60and80_20return()
        {
            int value1 = 180;
            int value2 = 240;
            int value3 = 40;
            int value4 = 60;
            int value5 = 80;
            int expected = 20;

            int actual = GreatestCommonDivisor.Euclidean(value1, value2, value3, value4, value5);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Stein_180and240_60return()
        {
            int value1 = 180;
            int value2 = 240;
            int expected = 60;

            int actual = GreatestCommonDivisor.Stein(value1, value2);

            Assert.AreEqual(expected, actual);
        }
    }
}
