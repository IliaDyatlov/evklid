﻿using System;

namespace Evklid
{
    class Program
    {
        static void Main(string[] args)
        {
            int euclideanTwo = GreatestCommonDivisor.Euclidean(180, 111);
            int euclideanTwoAndTime = GreatestCommonDivisor.Euclidean(180, 25, out TimeSpan euclideanTime);
            int euclideanThree = GreatestCommonDivisor.Euclidean(180, 240, 40);
            int euclideanFour = GreatestCommonDivisor.Euclidean(180, 240, 40, 60);
            int euclideanFive = GreatestCommonDivisor.Euclidean(180, 240, 40, 60, 80);

            int steinTwo = GreatestCommonDivisor.Stein(180, 240);
            int steinTwoAndTime = GreatestCommonDivisor.Stein(180, 25, out TimeSpan steinTime);

            Console.WriteLine($"GCD for two numbers: {euclideanTwo}");
            Console.WriteLine($"GCD for two numbers: {euclideanTwoAndTime}. Time: {euclideanTime}.");
            Console.WriteLine($"GCD for three numbers: { euclideanThree}");
            Console.WriteLine($"GCD for four numbers: { euclideanFour}");
            Console.WriteLine($"GCD for five numbers: { euclideanFive}");

            Console.WriteLine($"GCD for 2 numbers (Stein): {steinTwo}");
            Console.WriteLine($"GCD for 2 numbers (Stein): {steinTwoAndTime}. Time: {steinTime}.");
        }
    }
}
