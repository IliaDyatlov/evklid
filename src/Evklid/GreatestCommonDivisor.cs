﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Evklid
{
    public class GreatestCommonDivisor
    {
        public static int Euclidean(int value1, int value2)
        {
            return CalculateEuclidean(value1, value2);
        }

        public static int Euclidean(int value1, int value2, int value3)
        {
            return CalculateEuclidean(value1, value2, value3);
        }

        public static int Euclidean(int value1, int value2, int value3, int value4)
        {
            return CalculateEuclidean(value1, value2, value3, value4);
        }

        public static int Euclidean(int value1, int value2, int value3, int value4, int value5)
        {
            return CalculateEuclidean(value1, value2, value3, value4, value5);
        }

        private static int CalculateEuclidean(params int[] values) 
        {
            if (values == null || values.Length == 0)
            {
                throw new ArgumentNullException(nameof(values));
            }

            int divisor = 1;

            var listOfValues = values.OrderBy(x => x).ToList();

            for (int i = 2; i <= values.First(); i++)
            {
                if (values.All(x => x % i == 0))
                {
                    divisor = i;
                }
            }

            return divisor;
        }

        public static int Stein(int value1, int value2)
        {
            int divisor = 1;
            int temp;

            if (value1 == 0)
            {
                return value2;
            }

            if (value2 == 0)
            {
                return value1;
            }

            if (value1 == value2)
            {
                return value1;
            }

            if (value1 == 1 || value2 == 1)
            {
                return 1;
            }

            while (value1 != 0 && value2 != 0)
            {
                if (value1 % 2 == 0 && value2 % 2 == 0)
                {
                    divisor *= 2;
                    value1 /= 2;
                    value2 /= 2;
                    continue;
                }

                if (value1 % 2 == 0 && value2 % 2 != 0)
                {
                    value1 /= 2;
                    continue;
                }

                if (value1 % 2 != 0 && value2 % 2 == 0)
                {
                    value2 /= 2;
                    continue;
                }

                if (value1 > value2)
                {
                    temp = value1;
                    value1 = value2;
                    value2 = temp;
                }

                temp = value1;
                value1 = (value2 - value1) / 2;
                value2 = temp;
            }

            if (value1 == 0)
            {
                return divisor * value2;
            }
            else
            {
                return divisor * value1;
            }
        }

        public static int Euclidean(int value1, int value2, out TimeSpan time)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var result = Euclidean(value1, value2);

            stopwatch.Stop();
            time = stopwatch.Elapsed;

            return result;
        }

        public static int Stein(int value1, int value2, out TimeSpan time)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var result = Stein(value1, value2);

            stopwatch.Stop();
            time = stopwatch.Elapsed;

            return result;
        }
    }
}
